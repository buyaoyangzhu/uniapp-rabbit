# 登录模块

## 个人信息

> `src\pages\my\index.vue`

![image-20221128213527935](./assets/image-20221128213527935.png)



### 整体结构

```vue
<template>
  <scroll-view enhanced scroll-y id="scrollView">
    <view class="viewport" :style="{ paddingTop: 0 + 'px' }">
      <!-- 顶部背景 -->
      <!-- 个人资料 -->
      <!-- 订单 -->
      <!-- 部件 -->
      <!-- 商品列表 -->
    </view>
  </scroll-view>
</template>

<style lang="scss">
page {
  height: 100%;
  overflow: hidden;
  background-color: #f7f7f8;
}
#scrollView {
  height: 100%;
  overflow: hidden;
}
.viewport {
}
.navbar {
  width: 750rpx;
  height: 380rpx;
  background-image: url(http://static.botue.com/erabbit/static/images/center_bg.png);
  background-size: contain;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 9;
  .title {
    height: 64rpx;
    line-height: 64rpx;
    text-align: center;
    color: #fff;
    font-size: 32rpx;
    opacity: 0;
    font-weight: bold;
  }
}
.profile {
  position: relative;
  z-index: 99;
  .overview {
    display: flex;
    width: 500rpx;
    height: 120rpx;
    padding: 0 36rpx;
    color: #fff;
    .avatar {
      width: 120rpx;
      height: 120rpx;
      border-radius: 50%;
    }
    .meta {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: flex-start;
      line-height: 30rpx;
      padding: 16rpx 0;
      margin-left: 20rpx;
    }
  }
  .meta {
    .nickname {
      max-width: 180rpx;
      margin-bottom: 16rpx;
      font-size: 30rpx;
      overflow: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
    }
    .extra {
      display: flex;
      font-size: 20rpx;
    }
    .tips {
      font-size: 22rpx;
    }
  }
  .settings {
    position: absolute;
    top: 50%;
    right: 80rpx;
    line-height: 1;
    font-size: 30rpx;
    color: #fff;
    transform: translateY(-50%);
  }
}
.profile .meta .update,
.profile .meta .relogin {
  padding: 3rpx 10rpx 1rpx;
  color: rgba(255, 255, 255, 0.8);
  border: 1rpx solid rgba(255, 255, 255, 0.8);
  margin-right: 10rpx;
  border-radius: 30rpx;
}
.orders {
  position: sticky;
  top: 140rpx;
  z-index: 99;
  padding: 30rpx;
  margin: 50rpx 20rpx 0;
  background-color: #fff;
  border-radius: 10rpx;
  box-shadow: 0 4rpx 6rpx rgba(240, 240, 240, 0.6);
  .title {
    height: 40rpx;
    line-height: 40rpx;
    font-size: 28rpx;
    color: #1e1e1e;
    navigator {
      font-size: 24rpx;
      color: #939393;
      float: right;
    }
  }
  .section {
    width: 100%;
    display: flex;
    justify-content: space-between;
    padding: 40rpx 20rpx 10rpx;
    navigator {
      text-align: center;
      font-size: 24rpx;
      color: #333;
      &::before {
        display: block;
        font-size: 60rpx;
        color: #ff9545;
      }
    }
  }
}
/* 小部件 */
.widgets {
  padding: 20rpx 20rpx 0;
  background-color: #f7f7f8;
  position: relative;
  z-index: 1;
  .tabs {
    display: flex;
    justify-content: space-around;
    height: 80rpx;
    line-height: 80rpx;
    text {
      font-size: 28rpx;
      color: #333;
      position: relative;
    }
    .active {
      &::after {
        position: absolute;
        left: 50%;
        bottom: 12rpx;
        width: 60rpx;
        height: 4rpx;
        background-color: #27ba9b;
        content: "";
        transform: translate(-50%);
      }
    }
  }
}
</style>
```



### 顶部背景

![image-20221128113045905](./assets/image-20221128113045905.png)

```vue
  <view class="navbar" :style="{ paddingTop: 0 + 'px' }">
    <view class="title">我的</view>
  </view>
```



### 个人资料

![image-20221128113653788](./assets/image-20221128113653788.png)



1. 将登录信息存到vuex中，
2. 根据登录信息显示不同的界面
3. 点击 `登录`，跳转到 `登录页面`
4. 点击 `设置`  ，跳转到 `设置页面`
5. 点击 `头像` ， 跳转到 `个人信息修改页面`

```VUE
      <view class="profile">
        <view class="overview">
          <navigator v-if="profile.token" url="/pages/my/profile" hover-class="none">
            <image
              mode="aspectFill"
              class="avatar"
              :src="profile.avatar"
            ></image>
          </navigator>
          <!-- 未登录：点击头像跳转登录页 -->
          <navigator v-else url="/pages/login/index" hover-class="none">
            <image
              class="avatar"
              src="http://static.botue.com/erabbit/static/uploads/avatar_3.jpg"
            ></image>
          </navigator>
          <view class="meta">
            <view @tap="goToProfile" v-if="profile" class="nickname">
              {{ profile.nickname }}
            </view>
            <!-- 未登录：点击文字跳转登录页 -->
            <navigator
              v-else
              url="/pages/login/index"
              hover-class="none"
              class="nickname"
            >
              未登录
            </navigator>
            <view class="extra">
              <text v-if="!profile" class="tips">点击登录账号</text>
              <template v-else>
                <text class="update">更新头像昵称</text>
                <text class="relogin">切换账号</text>
              </template>
            </view>
          </view>
        </view>
        <navigator class="settings" url="/pages/my/settings" hover-class="none">
          设置
        </navigator>
      </view>
```



### 我的页面 订单 和 收藏数据

```js
  data() {
    return {
      orderTypes: [
        { text: "待付款", icon: "icon-currency", type: 1 },
        { text: "待发货", icon: "icon-gift", type: 2 },
        { text: "待收货", icon: "icon-check", type: 3 },
        { text: "待评价", icon: "icon-comment", type: 4 },
      ]
      tabs: ["我的收藏", "猜你喜欢", "我的足迹"],
      tabIndex: 0,
    };
  },
```



### 我的订单

![image-20221128115646627](./assets/image-20221128115646627.png)

#### 数据

```js
  data() {
    return {
      orderTypes: [
        { text: "待付款", icon: "icon-currency", type: 1 },
        { text: "待发货", icon: "icon-gift", type: 2 },
        { text: "待收货", icon: "icon-check", type: 3 },
        { text: "待评价", icon: "icon-comment", type: 4 },
      ]
    }
  },
```

#### 代码

```vue
  <view class="orders">
    <view class="title">
      我的订单
      <navigator url="/pages/order/index?type=0" hover-class="none">
        查看全部订单<text class="icon-right"></text>
      </navigator>
    </view>
    <view class="section">
      <navigator
        v-for="item in orderTypes"
        :key="item.text"
        :class="item.icon"
        :url="'/pages/order/index?type=' + item.type"
        hover-class="none"
        >{{ item.text }}</navigator
      >
      <navigator class="icon-handset" url=" " hover-class="none"
        >售后</navigator
      >
    </view>
  </view>
```





### 我的收藏

![image-20221128120215349](./assets/image-20221128120215349.png)



#### 静态结构 - 代码

```vue
  <view class="widgets">
    <view class="tabs">
      <text
        v-for="(item, index) in tabs"
        :key="item"
        :class="{ active: tabIndex === index }"
        @click="tabIndex =index"
        >{{ item }}</text
      >
    </view>
  </view>
```

### 商品列表 - 都是静态结构，接受外部数据 `tabIndex` 即可

> `src\pages\my\components\CollectGoods.vue`

![image-20221128120437655](./assets/image-20221128120437655.png)

1. 都是静态结构，接受外部数据 `tabIndex` 即可 

```vue
<template>
   <view class="masonry">
          <template v-if="tabIndex === 0">
            <view class="column">
              <view class="card topic">
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/topic_1.jpg"
                ></image>
                <view class="name">忙里忙外，回家吃饭</view>
                <view class="price">19.9元<text>起</text></view>
                <view class="extra">
                  <text class="icon-heart">1220</text>
                  <text class="icon-preview">1000</text>
                </view>
              </view>
              <view class="card brand">
                <view class="locate">
                  <text class="icon-locate"></text>中国
                </view>
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/brand_logo_6.jpg"
                ></image>
                <view class="name">小米优购</view>
                <view class="alt">小米优购闪购嗨购</view>
              </view>
              <view class="card goods">
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/goods_big_6.jpg"
                ></image>
                <view class="name"
                  >彩色鹅卵石小清新防水防烫长方形餐桌圆桌布艺茶几垫电视柜盖布
                  鹅软石桌布yg056</view
                >
                <view class="price">¥899</view>
              </view>
              <view class="card brand">
                <view class="locate">
                  <text class="icon-locate"></text>中国
                </view>
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/brand_logo_8.jpg"
                ></image>
                <view class="name">小米优购</view>
                <view class="alt">小米优购闪购嗨购</view>
              </view>
              <view class="card goods">
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/goods_big_7.jpg"
                ></image>
                <view class="name"
                  >彩色鹅卵石小清新防水防烫长方形餐桌圆桌布艺茶几垫电视柜盖布
                  鹅软石桌布yg056</view
                >
                <view class="price">¥899</view>
              </view>
            </view>
            <view class="column">
              <view class="card goods">
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/goods_big_5.jpg"
                ></image>
                <view class="name"
                  >彩色鹅卵石小清新防水防烫长方形餐桌圆桌布艺茶几垫电视柜盖布
                  鹅软石桌布yg056</view
                >
                <view class="price">¥899</view>
              </view>
              <view class="card brand">
                <view class="locate">
                  <text class="icon-locate"></text>中国
                </view>
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/brand_logo_7.jpg"
                ></image>
                <view class="name">小米优购</view>
                <view class="alt">小米优购闪购嗨购</view>
              </view>
              <view class="card topic">
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/topic_2.jpg"
                ></image>
                <view class="name">忙里忙外，回家吃饭</view>
                <view class="price">19.9元<text>起</text></view>
                <view class="extra">
                  <text class="icon-heart">1220</text>
                  <text class="icon-preview">1000</text>
                </view>
              </view>
              <view class="card brand">
                <view class="locate">
                  <text class="icon-locate"></text>中国
                </view>
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/brand_logo_5.jpg"
                ></image>
                <view class="name">小米优购</view>
                <view class="alt">小米优购闪购嗨购</view>
              </view>
              <view class="card goods">
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/goods_big_3.jpg"
                ></image>
                <view class="name"
                  >彩色鹅卵石小清新防水防烫长方形餐桌圆桌布艺茶几垫电视柜盖布
                  鹅软石桌布yg056</view
                >
                <view class="price">¥899</view>
              </view>
            </view>
          </template>
          <template v-if="tabIndex === 1">
            <view class="column">
              <view class="card goods">
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/goods_big_7.jpg"
                ></image>
                <view class="name"
                  >彩色鹅卵石小清新防水防烫长方形餐桌圆桌布艺茶几垫电视柜盖布
                  鹅软石桌布yg056</view
                >
                <view class="price">¥899</view>
              </view>
              <view class="card brand">
                <view class="locate">
                  <text class="icon-locate"></text>中国
                </view>
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/brand_logo_8.jpg"
                ></image>
                <view class="name">小米优购</view>
                <view class="alt">小米优购闪购嗨购</view>
              </view>
              <view class="card topic">
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/topic_1.jpg"
                ></image>
                <view class="name">忙里忙外，回家吃饭</view>
                <view class="price">19.9元<text>起</text></view>
                <view class="extra">
                  <text class="icon-heart">1220</text>
                  <text class="icon-preview">1000</text>
                </view>
              </view>
              <view class="card brand">
                <view class="locate">
                  <text class="icon-locate"></text>中国
                </view>
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/brand_logo_6.jpg"
                ></image>
                <view class="name">小米优购</view>
                <view class="alt">小米优购闪购嗨购</view>
              </view>
              <view class="card goods">
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/goods_big_6.jpg"
                ></image>
                <view class="name"
                  >彩色鹅卵石小清新防水防烫长方形餐桌圆桌布艺茶几垫电视柜盖布
                  鹅软石桌布yg056</view
                >
                <view class="price">¥899</view>
              </view>
            </view>
            <view class="column">
              <view class="card topic">
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/topic_2.jpg"
                ></image>
                <view class="name">忙里忙外，回家吃饭</view>
                <view class="price">19.9元<text>起</text></view>
                <view class="extra">
                  <text class="icon-heart">1220</text>
                  <text class="icon-preview">1000</text>
                </view>
              </view>
              <view class="card brand">
                <view class="locate">
                  <text class="icon-locate"></text>中国
                </view>
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/brand_logo_5.jpg"
                ></image>
                <view class="name">小米优购</view>
                <view class="alt">小米优购闪购嗨购</view>
              </view>
              <view class="card goods">
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/goods_big_5.jpg"
                ></image>
                <view class="name"
                  >彩色鹅卵石小清新防水防烫长方形餐桌圆桌布艺茶几垫电视柜盖布
                  鹅软石桌布yg056</view
                >
                <view class="price">¥899</view>
              </view>
              <view class="card brand">
                <view class="locate">
                  <text class="icon-locate"></text>中国
                </view>
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/brand_logo_7.jpg"
                ></image>
                <view class="name">小米优购</view>
                <view class="alt">小米优购闪购嗨购</view>
              </view>
              <view class="card goods">
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/goods_big_3.jpg"
                ></image>
                <view class="name"
                  >彩色鹅卵石小清新防水防烫长方形餐桌圆桌布艺茶几垫电视柜盖布
                  鹅软石桌布yg056</view
                >
                <view class="price">¥899</view>
              </view>
            </view>
          </template>
          <template v-if="tabIndex === 2">
            <view class="column">
              <view class="card goods">
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/goods_big_6.jpg"
                ></image>
                <view class="name"
                  >彩色鹅卵石小清新防水防烫长方形餐桌圆桌布艺茶几垫电视柜盖布
                  鹅软石桌布yg056</view
                >
                <view class="price">¥899</view>
              </view>
              <view class="card brand">
                <view class="locate">
                  <text class="icon-locate"></text>中国
                </view>
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/brand_logo_8.jpg"
                ></image>
                <view class="name">小米优购</view>
                <view class="alt">小米优购闪购嗨购</view>
              </view>
              <view class="card topic">
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/topic_1.jpg"
                ></image>
                <view class="name">忙里忙外，回家吃饭</view>
                <view class="price">19.9元<text>起</text></view>
                <view class="extra">
                  <text class="icon-heart">1220</text>
                  <text class="icon-preview">1000</text>
                </view>
              </view>
              <view class="card brand">
                <view class="locate">
                  <text class="icon-locate"></text>中国
                </view>
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/brand_logo_6.jpg"
                ></image>
                <view class="name">小米优购</view>
                <view class="alt">小米优购闪购嗨购</view>
              </view>
              <view class="card goods">
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/goods_big_7.jpg"
                ></image>
                <view class="name"
                  >彩色鹅卵石小清新防水防烫长方形餐桌圆桌布艺茶几垫电视柜盖布
                  鹅软石桌布yg056</view
                >
                <view class="price">¥899</view>
              </view>
            </view>
            <view class="column">
              <view class="card brand">
                <view class="locate">
                  <text class="icon-locate"></text>中国
                </view>
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/brand_logo_7.jpg"
                ></image>
                <view class="name">小米优购</view>
                <view class="alt">小米优购闪购嗨购</view>
              </view>
              <view class="card topic">
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/topic_2.jpg"
                ></image>
                <view class="name">忙里忙外，回家吃饭</view>
                <view class="price">19.9元<text>起</text></view>
                <view class="extra">
                  <text class="icon-heart">1220</text>
                  <text class="icon-preview">1000</text>
                </view>
              </view>
              <view class="card goods">
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/goods_big_5.jpg"
                ></image>
                <view class="name"
                  >彩色鹅卵石小清新防水防烫长方形餐桌圆桌布艺茶几垫电视柜盖布
                  鹅软石桌布yg056</view
                >
                <view class="price">¥899</view>
              </view>
              <view class="card brand">
                <view class="locate">
                  <text class="icon-locate"></text>中国
                </view>
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/brand_logo_5.jpg"
                ></image>
                <view class="name">小米优购</view>
                <view class="alt">小米优购闪购嗨购</view>
              </view>
              <view class="card goods">
                <image
                  mode="widthFix"
                  src="http://static.botue.com/erabbit/static/uploads/goods_big_3.jpg"
                ></image>
                <view class="name"
                  >彩色鹅卵石小清新防水防烫长方形餐桌圆桌布艺茶几垫电视柜盖布
                  鹅软石桌布yg056</view
                >
                <view class="price">¥899</view>
              </view>
            </view>
          </template>
        </view>
</template>  

<script>
export default {
  props:["tabIndex"]
}

</script>
<style lang="scss">
/* 瀑布流布局 */
.masonry {
  display: flex;
  justify-content: space-between;
  padding: 20rpx 0;
  .column {
    width: 345rpx;
    .card {
      padding: 20rpx 15rpx;
      margin-bottom: 20rpx;
      border-radius: 8rpx;
      background-color: #fff;
    }
  }
  .card {
    .name {
      font-size: 24rpx;
      color: #333;
      margin-top: 10rpx;
      overflow: hidden;
      text-overflow: ellipsis;
      display: -webkit-box;
      -webkit-line-clamp: 2;
      -webkit-box-orient: vertical;
    }
    .price {
      line-height: 1;
      font-size: 22rpx;
      color: #cf4444;
    }
    .locate {
      text-align: left;
      color: #999;
      margin-bottom: 10rpx;
      font-size: 24rpx;
    }
  }
  .topic {
    .price {
      margin: 10rpx 0;
      text {
        color: #999;
      }
    }
    .extra {
      line-height: 1;
      font-size: 22rpx;
      color: #666;
    }
  }
  .extra {
    text {
      margin-right: 14rpx;
      &::before {
        margin-right: 4rpx;
      }
    }
  }
  .icon-preview {
    &:before {
      font-size: 25rpx;
    }
  }
  .goods {
    .price {
      margin-top: 10rpx;
    }
  }
  .brand {
    text-align: center;
    .name {
      margin: 10rpx 0 8rpx;
    }
    .alt {
      line-height: 1;
      color: #666;
      font-size: 24rpx;
    }
  }
}
</style>
```

## 登录页面

> `src\pages\login\index.vue`

![image-20221128120643190](./assets/image-20221128120643190.png)

### 样式

```vue
<style lang="scss">
page {
  height: 100%;
}
.viewport {
  display: flex;
  flex-direction: column;
  height: 100%;
  padding: 20rpx 40rpx;
}
.logo {
  flex: 1;
  text-align: center;
  image {
    width: 220rpx;
    height: 220rpx;
    margin-top: 100rpx;
  }
}
.login {
  display: flex;
  flex-direction: column;
  height: 600rpx;
  padding: 40rpx 20rpx 20rpx;
  .button {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    height: 80rpx;
    font-size: 28rpx;
    border-radius: 72rpx;
    color: #fff;
    .icon {
      font-size: 40rpx;
      margin-right: 6rpx;
    }
  }
  .phone {
    background-color: #28bb9c;
  }
  .wechat {
    background-color: #06c05f;
  }
  .extra {
    flex: 1;
    padding: 70rpx 70rpx 0;
    .caption {
      width: 440rpx;
      line-height: 1;
      border-top: 1rpx solid #ddd;
      font-size: 26rpx;
      color: #999;
      position: relative;
    }
    .options {
      display: flex;
      justify-content: center;
      margin-top: 70rpx;
    }
    .icon {
      text-align: center;
      font-size: 28rpx;
      color: #444;
      &::before {
        display: flex;
        align-items: center;
        justify-content: center;
        width: 80rpx;
        height: 80rpx;
        margin-bottom: 6rpx;
        font-size: 40rpx;
        border: 1rpx solid #444;
        border-radius: 50%;
      }
    }
    .icon-weixin {
      &::before {
        border-color: #06c05f;
        color: #06c05f;
      }
    }
  }
  .caption {
    text {
      transform: translate(-40%);
      background-color: #fff;
      position: absolute;
      top: -12rpx;
      left: 50%;
    }
  }
  .options {
    button {
      line-height: 1;
      padding: 0;
      margin: 0 40rpx;
      background-color: transparent;
    }
  }
  .tips {
    position: absolute;
    bottom: 80rpx;
    left: 20rpx;
    right: 20rpx;
    font-size: 22rpx;
    color: #999;
    text-align: center;
  }
}
</style>

```

### 代码

```vue
<template>
  <view class="viewport">
    <view class="logo">
      <image
        src="https://static.botue.com/erabbit/static/images/logo_icon.png"
      ></image>
    </view>
    <view class="login">
      <button
        class="button phone"
        open-type="getPhoneNumber"
        @getphonenumber="handleGetPhoneNumber"
      >
        <text class="icon icon-phone"></text>
        手机号快捷登录
      </button>
      <view class="extra">
        <view class="caption">
          <text>其它登录方式</text>
        </view>
        <view class="options">
          <button @click="handleNextVersion">
            <text class="icon icon-weixin">微信</text>
          </button>
          <button @click="handleNextVersion">
            <text class="icon icon-phone">手机</text>
          </button>
          <button @click="handleNextVersion">
            <text class="icon icon-mail">邮箱</text>
          </button>
        </view>
      </view>
      <view class="tips"
        >登录/注册即视为你同意《服务条款》和《小兔鲜儿隐私协议》</view
      >
    </view>
  </view>
</template>
```

### 业务

1. 调用微信 `wx.login` 获取 临时登录凭据 `code`
2. 调用 微信 `获取手机号码` API 获取 手机号码数据 `encryptedData`、`iv` 
3. 调用 登录接口，获取token
4. 存入vuex中，并返回上一个页面





## 修改个人信息

> `src/pages/my/profile.vue`

![image-20221128161851732](./assets/image-20221128161851732.png)

### 样式

```vue
<template>
  <view class="viewport">
    <!-- 顶部背景 -->
    <view class="navbar" :style="{ paddingTop: safeArea.top + 'px' }">
      <view class="back icon-left" @click="goBack"></view>
      <view class="title">个人信息</view>
    </view>
    <scroll-view scroll-y>
      <!-- 头像 -->
      <view class="avatar" @tap="changeAvatar">
        <image :src="memberProfile.avatar" mode="aspectFill" />
        <text>点击修改头像</text>
      </view>
      <view class="form">
        <view class="form-item">
          <text class="label">账号</text>
          <!-- 账号名不能修改，用 text 组件展示 -->
          <text>{{ memberProfile.account }}</text>
        </view>
        <view class="form-item">
          <text class="label">昵称</text>
          <!-- 输入框通过 v-model 双向绑定修改数据 -->
          <input v-model="memberProfile.nickname" />
        </view>
        <view class="form-item">
          <text class="label">性别</text>
          <radio-group @change="genderChange">
            <label class="radio">
              <radio
                value="男"
                color="#27ba9b"
                :checked="memberProfile.gender === '男'"
              />
              男
            </label>
            <label class="radio">
              <radio
                value="女"
                color="#27ba9b"
                :checked="memberProfile.gender === '女'"
              />
              女
            </label>
          </radio-group>
        </view>
        <view class="form-item">
          <text class="label">出生日期</text>
          <picker
            @change="handleBirthdayChange"
            mode="date"
            start="1900-01-01"
            end="2022-01-01"
          >
            <view>{{ memberProfile.birthday || "请选择日期" }}</view>
          </picker>
        </view>
        <view class="form-item">
          <text class="label">城市</text>
          <picker @change="handleFullLocationChange" mode="region">
            <view>{{ memberProfile.fullLocation || "请选择城市" }}</view>
          </picker>
        </view>
        <view class="form-item">
          <text class="label">职业</text>
          <!-- 输入框通过 v-model 双向绑定修改数据 -->
          <input v-model="memberProfile.profession" placeholder="请填写职业" />
        </view>
        <!-- 提交按钮 -->
        <view class="button" @click="handleSubmitForm">保 存</view>
      </view>
    </scroll-view>
  </view>
</template>

<style lang="scss">
page {
  height: 100%;
  overflow: hidden;
  background-color: #f4f4f4;
}
.viewport {
  display: flex;
  flex-direction: column;
  height: 100%;
  background-image: url(https://static.botue.com/erabbit/static/images/order_bg.png);
  background-size: auto 392rpx;
  background-repeat: no-repeat;
}
.navbar {
  .title {
    height: 40px;
    line-height: 32px;
    text-align: center;
    font-size: 17px;
    font-weight: 500;
    color: #fff;
  }
  .back {
    position: absolute;
    left: 20rpx;
    top: 22px;
    font-size: 23px;
    z-index: 9;
    color: #fff;
  }
}
.avatar {
  text-align: center;
  padding: 20rpx 0 40rpx;
  image {
    width: 160rpx;
    height: 160rpx;
    border-radius: 50%;
  }
  text {
    display: block;
    padding-top: 20rpx;
    line-height: 1;
    font-size: 26rpx;
    color: #fff;
  }
}
.form {
  margin: 20rpx 20rpx 0;
  padding: 0 20rpx;
  border-radius: 10rpx;
  background-color: #fff;
  .form-item {
    display: flex;
    height: 96rpx;
    line-height: 46rpx;
    padding: 25rpx 10rpx;
    background-color: #fff;
    font-size: 28rpx;
    border-bottom: 1rpx solid #ddd;
    &:last-child {
      border: none;
    }
    .label {
      width: 180rpx;
      color: #333;
    }
    input {
      flex: 1;
      display: block;
      height: 46rpx;
    }
    .radio {
      display: inline-block;
      height: 46rpx;
      margin-right: 20rpx;
      vertical-align: middle;
    }
    radio {
      transform: scale(0.7) translateY(-2px);
    }
    picker {
      flex: 1;
    }
  }
}
.button {
  height: 80rpx;
  text-align: center;
  line-height: 80rpx;
  margin: 30rpx 20rpx;
  color: #fff;
  border-radius: 80rpx;
  font-size: 30rpx;
  background-color: #27ba9b;
}
</style>
```

### 

### 数据说明

1. `memberProfile`  用来绑定表单的数据对象
2. `changeAvatar` 修改头像的方法
   1. 调用小程序内置的 `chooseImage` API 拍摄或者选择照片，现在建议使用`chooseMedia`
   2. 拿到临时地址，发送给后端

3. `genderChange` 修改性别的方法
4. `handleBirthdayChange`修改生日的方法
5. `handleFullLocationChange` 根据邮编设置详细地址的方法
6. `handleSubmitForm` 表单的提交事件
7. `picker` 小程序中 内置的多列选择组件



### 业务

1. 打开页面，获取当前登录用户个人信息，设置到 data中，进而在表单中显示出来
2. 完成表单的一系列按钮功能
3. 点击提交表单，完成个人信息的修改 