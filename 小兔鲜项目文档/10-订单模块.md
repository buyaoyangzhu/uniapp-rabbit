# 订单模块

## 创建订单

> `src\pages\order\create\index.vue`

![image-20221204001914040](assets/image-20221204001914040.png)

### 静态结构

```vue
<template>
  <view class="order-create">
    <scroll-view scroll-y class="viewport">
      <!-- 收货地址 -->
      <navigator
        class="shipment"
        hover-class="none"
        url="/pages/my/address/index?from=order"
        v-if="selectedAddress"
      >
        <view class="user">
          {{ selectedAddress.receiver }} {{ selectedAddress.contact }}
        </view>
        <view class="address">
          {{ selectedAddress.fullLocation }}{{ selectedAddress.address }}
        </view>
        <text class="icon icon-right"></text>
      </navigator>

      <!-- 没有收货地址 -->
      <navigator
        class="shipment"
        hover-class="none"
        url="/pages/my/address/index?from=order"
        v-else
      >
        <view class="address"> 请选择收货地址 </view>
        <text class="icon icon-right"></text>
      </navigator>

      <!-- 商品信息 -->
      <view class="goods">
        <navigator
          v-for="item in orderPre.goods"
          :key="item.skuId"
          :url="`/pages/goods/index?id=${item.id}`"
          class="item"
          hover-class="none"
        >
          <image class="cover" :src="item.picture"></image>
          <view class="meta">
            <view class="name ellipsis">
              {{ item.name }}
            </view>
            <view class="type">{{ item.attrsText }}</view>
            <view class="price">
              <view class="actual">
                <text class="symbol">¥</text>{{ item.payPrice }}
              </view>
              <view class="original">
                <text class="symbol">¥</text>{{ item.price }}
              </view>
            </view>
            <view class="quantity">x{{ item.count }}</view>
          </view>
        </navigator>
      </view>

      <!-- 配送及支付方式 -->
      <view class="related">
        <view class="item">
          <text class="text">配送时间</text>
          <text class="picker icon-fonts">{{ payments[0].text }}</text>
        </view>
        <view class="item">
          <text class="text">支付方式</text>
          <text class="picker icon-fonts">{{ shipments[0].text }}</text>
        </view>
        <view class="item">
          <text class="text">买家备注</text>
          <input
            v-model="buyerMessage"
            cursor-spacing="30"
            placeholder="建议留言前先与商家沟通确认"
          />
        </view>
      </view>

      <!-- 支付金额 -->
      <view class="settlement">
        <view class="item">
          <text class="text">商品总价: </text>
          <text class="number">
            <text class="symbol">¥</text>
            {{ orderPre.summary.totalPrice }}
          </text>
        </view>
        <view class="item">
          <text class="text">运费: </text>
          <text class="number">
            <text class="symbol">¥</text>
            {{ orderPre.summary.postFee }}
          </text>
        </view>
        <!-- 如果有折扣，渲染折扣金额，没有折扣就不渲染了，免得用户伤心 -->
        <view class="item" v-if="orderPre.summary.discountPrice > 0">
          <text class="text">折扣: </text>
          <text class="number danger">
            <text class="symbol">-¥</text>
            {{ orderPre.summary.discountPrice }}
          </text>
        </view>
      </view>
    </scroll-view>

    <!-- 底部工具 -->
    <view class="toolbar">
      <view class="amount">
        <text class="symbol">¥</text>
        <text class="number">{{ orderPre.summary.totalPayPrice }}</text>
      </view>
      <view @click="submitForm" class="button">提交订单</view>
    </view>
  </view>
</template>

<script>
import { mapState } from 'vuex';

export default {
  computed: {
  },
  data() {
    return {
      // 预付订单列表
      orderPre: null,
      // 配送时间
      payments: [
        {
          id: 1,
          text: '时间不限 (周一至周日)',
        },
        {
          id: 2,
          text: '工作日送 (周一至周五)',
        },
        {
          id: 3,
          text: '周末配送 (周六至周日)',
        },
      ],
      // 支付方式
      shipments: [
        {
          id: 1,
          text: '在线支付',
        },
        {
          id: 2,
          text: '货到付款',
        },
      ],
      // 买家留言
      buyerMessage: '',
    };
  },
};
</script>

<style lang="scss">
.safe-area-bottom {
  background-color: #fff;
}
.order-create {
  display: flex;
  flex-direction: column;
  height: 100vh;
  overflow: hidden;
  background-color: #f4f4f4;
}

.viewport {
  padding-top: 20rpx;
}

.shipment {
  padding: 30rpx 30rpx 25rpx 84rpx;
  margin: 0 20rpx;
  font-size: 26rpx;
  border-radius: 10rpx;
  background: url(https://static.botue.com/erabbit/static/images/locate.png)
    20rpx center / 50rpx no-repeat #fff;
  position: relative;
}

.shipment .icon {
  font-size: 36rpx;
  color: #333;
  transform: translateY(-50%);
  position: absolute;
  top: 50%;
  right: 20rpx;
}

.shipment .user {
  color: #333;
  margin-bottom: 5rpx;
}

.shipment .address {
  color: #666;
}

.link {
  margin: 20rpx;
  text-align: center;
  line-height: 72rpx;
  font-size: 26rpx;
  color: #fff;
  border-radius: 72rpx;
  background-color: #27ba9b;
}

.popup-root {
  .list {
    padding: 20rpx 0 40rpx 10rpx !important;
  }

  .list .item {
    padding: 30rpx 60rpx 30rpx 10rpx;
    position: relative;
  }

  .list .item .icon {
    color: #999;
    font-size: 40rpx;
    transform: translateY(-50%);
    position: absolute;
    top: 50%;
    right: 10rpx;
  }

  .list .item .icon-checked {
    color: #27ba9b;
  }

  .list .item .text {
    font-size: 28rpx;
    color: #444;
  }
}

.goods {
  margin: 20rpx 20rpx 0;
  padding: 0 20rpx;
  border-radius: 10rpx;
  background-color: #fff;
}

.goods .item {
  display: flex;
  padding: 30rpx 0;
  border-top: 1rpx solid #eee;
}

.goods .item:first-child {
  border-top: none;
}

.goods .item .cover {
  width: 170rpx;
  height: 170rpx;
  border-radius: 10rpx;
  margin-right: 20rpx;
}

.goods .item .meta {
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  position: relative;
}

.goods .item .name {
  height: 80rpx;
  font-size: 26rpx;
  color: #444;
}

.goods .item .type {
  line-height: 1.8;
  padding: 0 15rpx;
  margin-top: 6rpx;
  font-size: 24rpx;
  align-self: flex-start;
  border-radius: 4rpx;
  color: #888;
  background-color: #f7f7f8;
}

.goods .item .price {
  display: flex;
  align-items: baseline;
  margin-top: 6rpx;
  font-size: 28rpx;
}

.goods .item .symbol {
  font-size: 20rpx;
}

.goods .item .original {
  font-size: 24rpx;
  color: #999;
  text-decoration: line-through;
}

.goods .item .actual {
  margin-right: 10rpx;
  color: #cf4444;
}

.goods .item .quantity {
  position: absolute;
  bottom: 0;
  right: 0;
  font-size: 26rpx;
  color: #444;
}

.related {
  padding: 0 20rpx;
  margin: 20rpx 20rpx 0;
  border-radius: 10rpx;
  background-color: #fff;
}

.related .item {
  display: flex;
  justify-content: space-between;
  align-items: center;
  min-height: 80rpx;
  font-size: 26rpx;
  color: #333;
}

.related input {
  flex: 1;
  text-align: right;
  margin: 20rpx 0;
  padding-right: 20rpx;
  font-size: 26rpx;
  color: #999;
}

.related .item .text {
  width: 125rpx;
}

.related .picker {
  color: #666;
}

.related .picker::after {
  content: '\e6c2';
}

/* 结算清单 */
.settlement {
  padding: 0 20rpx;
  margin: 20rpx 20rpx 0;
  border-radius: 10rpx;
  background-color: #fff;
}

.settlement .item {
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 80rpx;
  font-size: 26rpx;
  color: #333;
}

.settlement .symbol {
  font-size: 80%;
}

.settlement .danger {
  color: #cf4444;
}

.gap {
  height: 20rpx;
}

.toolbar {
  height: 120rpx;
  padding: 0 40rpx;
  border-top: 1rpx solid #eaeaea;
  background-color: #fff;
  display: flex;
  justify-content: space-between;
  align-items: center;
}

.toolbar .amount {
  font-size: 40rpx;
  color: #cf4444;
}

.toolbar .amount .symbol,
.toolbar .amount .decimal {
  font-size: 75%;
}

.toolbar .button {
  width: 220rpx;
  text-align: center;
  line-height: 72rpx;
  font-size: 26rpx;
  color: #fff;
  border-radius: 72rpx;
  background-color: #27ba9b;
}
</style>

```

### 收货地址

![image-20221204003146339](assets/image-20221204003146339.png)



### 没有收货地址

![image-20221204003402998](assets/image-20221204003402998.png)

### 商品信息

![image-20221204003956217](assets/image-20221204003956217.png)

### 配送及支付方式

![image-20221204004017212](assets/image-20221204004017212.png)



### 支付金额

![image-20221204004107015](assets/image-20221204004107015.png)



### 底部工具

![image-20221204004237516](assets/image-20221204004237516.png)



### 用到的数据

```js
computed: {
    ...mapState("address", ["selectedAddress"]),// 当前选择的收货地址
},
data() {
    return {
      // 预付订单列表
      orderPre: null,
      // 配送时间
      payments: [
        {
          id: 1,
          text: "时间不限 (周一至周日)",
        },
        {
          id: 2,
          text: "工作日送 (周一至周五)",
        },
        {
          id: 3,
          text: "周末配送 (周六至周日)",
        },
      ],
      // 支付方式
      shipments: [
        {
          id: 1,
          text: "在线支付",
        },
        {
          id: 2,
          text: "货到付款",
        },
      ],
      // 买家留言
      buyerMessage: "",
    };
  },
```

### 业务

1. 可以选择收货地址
2. 点击提交订单
   1. 判断有没有收货地址
   2. 拼接接口要求的参数，创建订单，跳转到订单详情页面





## 订单详情

> `src\pages\order\detail.vue`

![image-20221204002816294](assets/image-20221204002816294.png)

### 样式文件

```vue
<style lang="scss">
.order-detail {
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
  overflow: hidden;
}
.navbar {
  width: 750rpx;
  color: #fff;
  background-image: url(https://static.botue.com/erabbit/static/images/order_bg.png);
  background-size: cover;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 9;
  .title {
    height: 40px;
    line-height: 30px;
    text-align: center;
    font-size: 17px;
    font-weight: 500;
  }
  .android {
    text-align: left;
    padding-left: 42px;
  }
  .wrap {
    position: relative;
  }
  .back {
    position: absolute;
    left: 10px;
    top: 4px;
    line-height: 1;
    font-size: 23px;
    z-index: 9;
  }
}
.viewport {
  background-color: #f7f7f8;
}
.overview {
  display: flex;
  flex-direction: column;
  align-items: center;
  line-height: 1;
  padding-bottom: 30rpx;
  color: #fff;
  background-image: url(https://static.botue.com/erabbit/static/images/order_bg.png);
  background-size: cover;
  .status {
    font-size: 36rpx;
    &::before {
      margin-right: 6rpx;
      font-weight: 500;
    }
  }
  .tips {
    margin-top: 30rpx;
    font-size: 24rpx;
    width: 100%;
    text-align: center;
    .countdown {
      margin-left: 10rpx;
    }
  }
  .button {
    width: 260rpx;
    height: 64rpx;
    text-align: center;
    line-height: 64rpx;
    margin-top: 30rpx;
    font-size: 28rpx;
    color: #27ba9b;
    border-radius: 68rpx;
    background-color: #fff;
  }
}
.shipment {
  line-height: 1.4;
  padding: 0 20rpx;
  margin: 20rpx 20rpx 0;
  border-radius: 10rpx;
  background-color: #fff;
  .locate {
    background-image: url(https://static.botue.com/erabbit/static/images/locate.png);
    .user {
      font-size: 26rpx;
      color: #444;
    }
    .address {
      font-size: 24rpx;
      color: #666;
    }
  }
  .logistics {
    background-image: url(https://static.botue.com/erabbit/static/images/car.png);
    border-bottom: 1rpx solid #eee;
    position: relative;
    &::after {
      position: absolute;
      right: 10rpx;
      top: 50%;
      transform: translateY(-50%);
      content: "\e6c2";
      font-family: "erabbit" !important;
      font-size: 32rpx;
      color: #666;
    }
    .message {
      font-size: 26rpx;
      color: #444;
    }
    .date {
      font-size: 24rpx;
      color: #666;
    }
  }
}
.shipment .locate,
.shipment .logistics {
  min-height: 120rpx;
  padding: 30rpx 30rpx 25rpx 75rpx;
  background-size: 50rpx;
  background-repeat: no-repeat;
  background-position: 6rpx center;
}
.goods {
  margin: 20rpx 20rpx 0;
  padding: 0 20rpx;
  border-radius: 10rpx;
  background-color: #fff;
  .item {
    padding: 30rpx 0;
    border-bottom: 1rpx solid #eee;
    navigator {
      display: flex;
    }
    .cover {
      width: 170rpx;
      height: 170rpx;
      border-radius: 10rpx;
      margin-right: 20rpx;
    }
    .meta {
      flex: 1;
      display: flex;
      flex-direction: column;
      justify-content: center;
      position: relative;
    }
    .name {
      height: 80rpx;
      font-size: 26rpx;
      color: #444;
    }
    .type {
      line-height: 1.8;
      padding: 0 15rpx;
      margin-top: 6rpx;
      font-size: 24rpx;
      align-self: flex-start;
      border-radius: 4rpx;
      color: #888;
      background-color: #f7f7f8;
    }
    .price {
      display: flex;
      margin-top: 6rpx;
      font-size: 24rpx;
    }
    .symbol {
      font-size: 20rpx;
    }
    .original {
      color: #999;
      text-decoration: line-through;
    }
    .actual {
      margin-left: 10rpx;
      color: #444;
    }
    .text {
      font-size: 22rpx;
    }
    .quantity {
      position: absolute;
      bottom: 0;
      right: 0;
      font-size: 24rpx;
      color: #444;
    }
    .action {
      display: flex;
      flex-direction: row-reverse;
      justify-content: flex-start;
      padding: 30rpx 0 0;
    }
  }
  .action {
    .button {
      width: 200rpx;
      height: 60rpx;
      text-align: center;
      justify-content: center;
      line-height: 60rpx;
      margin-left: 20rpx;
      border-radius: 60rpx;
      border: 1rpx solid #ccc;
      font-size: 26rpx;
      color: #444;
    }
    .primary {
      color: #27ba9b;
      border-color: #27ba9b;
    }
  }
  .total {
    line-height: 1;
    font-size: 26rpx;
    padding: 20rpx 0;
    color: #666;
    .row {
      display: flex;
      align-items: center;
      justify-content: space-between;
      padding: 10rpx 0;
    }
    .symbol {
      &::before {
        content: "¥";
        font-size: 20rpx;
      }
    }
    .paid {
      font-size: 30rpx;
      color: #444;
    }
    .primary {
      color: #cf4444;
    }
  }
}
.detail {
  line-height: 1;
  padding: 30rpx 20rpx 0;
  margin: 20rpx 20rpx 0;
  font-size: 26rpx;
  color: #666;
  border-radius: 10rpx;
  background-color: #fff;
  .title {
    font-size: 30rpx;
    color: #444;
  }
  .row {
    padding: 20rpx 0;
    & > view {
      display: block;
      padding: 10rpx 0;
    }
  }
}
.toobar {
  display: flex;
  flex-direction: row-reverse;
  justify-content: flex-start;
  padding: 30rpx 20rpx;
  background-color: #fff;
  box-shadow: 0 -4rpx 6rpx rgba(240, 240, 240, 0.6);
  position: relative;
  z-index: 9;
  & > view {
    width: 200rpx;
    height: 72rpx;
    text-align: center;
    line-height: 72rpx;
    margin-left: 15rpx;
    font-size: 26rpx;
    border-radius: 72rpx;
  }
  .default {
    color: #444;
    border: 1rpx solid #ccc;
  }
  .primary {
    color: #fff;
    background-color: #27ba9b;
  }
}
.popup-root {
  padding: 30rpx 30rpx 0;
  border-radius: 10rpx 10rpx 0 0;
  overflow: hidden;
  .title {
    font-size: 30rpx;
    text-align: center;
    margin-bottom: 30rpx;
  }
  .description {
    font-size: 28rpx;
    padding: 0 20rpx;
    .tips {
      color: #444;
      margin-bottom: 12rpx;
    }
    .cell {
      display: flex;
      justify-content: space-between;
      align-items: center;
      line-height: 1;
      padding: 15rpx 0;
      margin-bottom: 4rpx;
      color: #666;
    }
    .icon-ring {
      font-size: 38rpx;
      color: #999;
    }
    .icon-checked {
      font-size: 38rpx;
      color: #27ba9b;
    }
  }
  .footer {
    display: flex;
    justify-content: space-between;
    padding: 30rpx 0 40rpx;
    font-size: 28rpx;
    color: #444;
    .button {
      flex: 1;
      height: 72rpx;
      text-align: center;
      line-height: 72rpx;
      margin: 0 20rpx;
      color: #444;
      border-radius: 72rpx;
      border: 1rpx solid #ccc;
    }
    .primary {
      color: #fff;
      background-color: #27ba9b;
      border: none;
    }
  }
}
</style>
```

### 基本结构

```vue
<template>
  <view class="order-detail">
    <!-- 顶部导航栏 -->

    <!-- 内容 -->
    <scroll-view class="viewport" scroll-y>
      <!-- 订单状态 -->
    

      <!-- 商品信息 -->
    

      <!-- 订单信息 -->
  
    </scroll-view>

    <!-- 底部工具 -->

  </view>
</template>
```

### 顶部导航栏

![image-20221204110608543](assets/image-20221204110608543.png)



```vue
<!-- 顶部导航栏 -->
<view class="navbar" :style="{ paddingTop: safeArea.top + 'px' }">
  <view class="wrap">
    <navigator open-type="navigateBack" class="back icon-left"></navigator>
    <view :class="['title', platform]">订单详情</view>
  </view>
</view>
```

### 订单状态

![image-20221204110814544](assets/image-20221204110814544.png)

```vue
<!-- 订单状态 -->
<view class="overview" :style="{ paddingTop:safeArea.top + 40 + 'px' }">
    <template v-if="order.orderState === OrderState.DaiFuKuan">
<view class="status icon-clock">等待付款</view>
<view class="tips">
    <text>应付金额: ¥{{ order.payMoney }}</text>
    <text class="countdown"> 支付剩余 {{ countdown }} </text>
        </view>
<view @tap="orderPay" class="button">去支付</view>
    </template>
    <template v-else>
<view class="status icon-clock">
    {{ OrderStateOptions[order.orderState] }}
        </view>
    </template>
</view>

```

### 商品信息

![image-20221204111807089](assets/image-20221204111807089.png)



```vue
      <!-- 商品信息 -->
      <view class="goods">
        <view class="item">
          <navigator
            v-for="item in order.skus"
            :key="item.id"
            :url="`/pages/goods/index?id=${item.spuId}`"
            hover-class="none"
          >
            <image class="cover" :src="item.image"></image>
            <view class="meta">
              <view class="name ellipsis">{{ item.name }}</view>
              <view class="type">{{ item.attrsText }}</view>
              <view class="price">
                <view class="original">
                  <text class="symbol">¥</text>
                  <text>{{ item.curPrice }}</text>
                </view>
                <view class="actual">
                  <text class="text">实付: </text>
                  <text class="symbol">¥</text>
                  <text>{{ item.realPay }}</text>
                </view>
              </view>
              <view class="quantity">x{{ item.quantity }}</view>
            </view>
          </navigator>
          <view
            class="action"
            v-if="order.orderState === OrderState.YiWanCheng"
          >
            <view class="button primary">申请售后</view>
            <navigator url="/pages/comments/publish/index" class="button">
              去评价
            </navigator>
          </view>
        </view>
        <!-- 合计 -->
        <view class="total">
          <view class="row">
            <view class="text">商品总价: </view>
            <view class="symbol">{{ order.totalMoney }}</view>
          </view>
          <view class="row">
            <view class="text">运费: </view>
            <view class="symbol">{{ order.postFee }}</view>
          </view>
          <view class="row paid">
            <view class="text">实付: </view>
            <view class="symbol primary">{{ order.payMoney }}</view>
          </view>
        </view>
      </view>

```

### 订单信息

![image-20221204111837162](assets/image-20221204111837162.png)

```vue
  <!-- 订单信息 -->
  <view class="detail">
    <view class="title">订单信息</view>
    <view class="row">
      <view>订单编号: {{ order.id }}</view>
      <view>下单时间: {{ order.createTime }}</view>
      <view v-if="order.payType === PayType.Online">
        支付方式: 在线支付
      </view>
      <view v-if="order.payChannel == PayChannel.WxPay">
        支付渠道: 微信支付
      </view>
    </view>
  </view>
```



### 订单相关常量

> `src\pages\order\OrderConstance.js`

```js
// 订单状态
// 订单状态，0为全部 1为待付款、2为待发货、3为待收货、4为待评价、5为已完成、6为已取消
export const OrderState = {
  QuanBu: 0,
  DaiFuKuan: 1,
  DaiFaHuo: 2,
  DaiShouHuo: 3,
  DaiPingJia: 4,
  YiWanCheng: 5,
  YiQuXiao: 6,
};
// 支付类型
export const PayType = {
  Online: 1,
  Delivery: 2,
};
// 支付渠道
export const PayChannel = {
  AliPay: 1,
  WxPay: 2,
};
// 订单状态描述
export const OrderStateOptions = {
  [OrderState.DaiFuKuan]: "待付款",
  [OrderState.DaiFaHuo]: "待发货",
  [OrderState.DaiShouHuo]: "待收货",
  [OrderState.DaiPingJia]: "待评价",
  [OrderState.YiWanCheng]: "已完成",
  [OrderState.YiQuXiao]: "已取消",
};
```

### 底部工具

![image-20221204125353055](assets/image-20221204125353055.png)

```vue
<template>
  <view class="order-detail">
    <!-- 顶部导航栏 -->

    <!-- 内容 -->
    <scroll-view class="viewport" scroll-y>
      <!-- 订单状态 -->
    

      <!-- 商品信息 -->
    

      <!-- 订单信息 -->
  
    </scroll-view>

    <!-- 底部工具 -->
    <view class="toobar" v-if="order.orderState === OrderState.DaiFuKuan">
      <view @tap="orderPay" class="primary">去支付</view>
      <view class="default">取消订单</view>
    </view>

  </view>
</template>
```





### 用到的数据

```js

  data() {
    return {
      // 订单信息
      order: null,
      // 订单状态
      OrderState,
      // 支付方式
      PayType,
      // 支付渠道
      PayChannel,
      // 订单状态描述
      OrderStateOptions,
    };
```

### 业务

1. 打开页面时

   1. 根据订单id，获取订单详情数据
   2. 判断订单的倒计时 `countdown`，如果小于 0，设置为 `已取消` 状态
   3. 否则开始 倒计时 利用 dayjs的 `unix`  和 `format` 格式化事件

2. 微信支付：https://pay.weixin.qq.com/wiki/doc/apiv3_partner/open/pay/chapter2_8_2.shtml#part-4

   1. 调用后端接口，获取微信支付所需参数
   2. 调用 `uni.requestPayment`  调起微信支付
   3. 支付成功，弹出提示，跳转到支付成功页面 `/pages/order/payment`
   4. 支付失败，跳转到订单列表页面  `/pages/order/index`

   

   

## 支付成功

> `src\pages\order\payment.vue`

![image-20221204130614159](assets/image-20221204130614159.png)



### 代码

只需要实现

1. 返回首页
2. 跳转到订单页面

```vue
<template>
  <view class="payment">
    <view class="navbar" :style="{ paddingTop: safeArea.top + 'px' }">
      <view class="wrap">
        <navigator class="back icon-left" @click="goBack"></navigator>
        <view :class="['title', platform]">支付成功</view>
      </view>
    </view>

    <scroll-view
      class="viewport"
      id="scrollView"
      enhanced
      scroll-y
      :show-scrollbar="false"
    >
      <!-- 订单状态 -->
      <view
        class="overview"
        :style="{ paddingTop: safeArea.top + 40 + 'px' }"
      >
        <view class="status icon-checked">支付成功</view>
        <view class="buttons">
          <navigator
            hover-class="none"
            class="button"
            open-type="switchTab"
            url="/pages/index/index"
          >
            返回首页
          </navigator>
          <navigator hover-class="none" url="/pages/order/index">
            查看订单
          </navigator>
        </view>
      </view>
    </scroll-view>
  </view>
</template>
<script>
import { mapState } from "vuex";
export default {
  computed: {
    ...mapState(["safeArea","platform"]),
  },
  data() {
    return {};
  },
};
</script>
<style lang="scss">
.payment {
  display: flex;
  flex-direction: column;
  height: 100%;
  overflow: hidden;
}

.navbar {
  width: 750rpx;
  color: #fff;
  background-color: #27ba9b;

  position: fixed;
  top: 0;
  left: 0;
  z-index: 9;
}

.navbar .title {
  height: 40px;
  line-height: 30px;
  text-align: center;
  font-size: 17px;
  font-weight: 500;
  opacity: 0;
}

.navbar .android {
  text-align: left;
  padding-left: 42px;
}

.navbar .wrap {
  position: relative;
}

.navbar .back {
  position: absolute;
  left: 10px;
  top: 4px;
  line-height: 1;
  font-size: 23px;
  z-index: 9;
}

.viewport {
  background-color: #f7f7f8;
}

.overview {
  line-height: 1;
  padding-bottom: 70rpx;
  color: #fff;
  background-color: #27ba9b;
}

.overview .status {
  font-size: 36rpx;
  font-weight: 500;
  text-align: center;
}

.overview .status::before {
  display: block;
  font-size: 110rpx;
  margin-bottom: 20rpx;
}

.overview .buttons {
  height: 60rpx;
  line-height: 60rpx;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 60rpx;
}

.overview navigator {
  text-align: center;
  margin: 0 10rpx;
  font-size: 28rpx;
  color: #fff;

  &:first-child {
    width: 200rpx;
    border-radius: 64rpx;
    border: 1rpx solid #fff;
  }
}
</style>

```

## 订单列表

> `src\pages\order\index.vue`

![image-20221204130851617](assets/image-20221204130851617.png)



### 基本结构

```vue
<template>
  <view class="viewport">
    <!-- 标题 -->
    <view class="tabs">
      <text
        v-for="(item, index) in orderTabs"
        :key="item.title"
        :data-index="index"
        @tap="activeIndex = index"
      >
        {{ item.title }}
      </text>
      <!-- 游标 -->
      <view
        class="cursor"
        :style="{ left: `calc(${activeIndex} * 20%)` }"
      ></view>
    </view>

    <!-- 内容 -->
    <swiper class="orders" :current="activeIndex" @change="swiperChange">
      <swiper-item v-for="(orders, index) in orderList" :key="index">
        <scroll-view scroll-y @scrolltolower="onScrolltolower">
          <view class="card" v-for="item in orders.items" :key="item.id">
            <!-- 订单相关 -->
            <view class="status">
              <text class="date">{{ item.createTime }}</text>
              <!-- 
                🔔 常量取值
              -->
              <text>{{ OrderStateOptions[item.orderState] }}</text>
              <text class="icon-delete"></text>
            </view>
            <!-- 商品信息 -->
            <navigator
              v-for="sku in item.skus"
              :key="sku.id"
              class="goods"
              :url="`/pages/order/detail?id=${item.id}`"
              hover-class="none"
            >
              <view class="cover">
                <image :src="sku.image"></image>
              </view>
              <view class="meta">
                <view class="name ellipsis">{{ sku.name }}</view>
                <view class="type">{{ sku.attrsText }}</view>
              </view>
            </navigator>
            <!-- 支付信息 -->
            <view class="payment">
              <text class="quantity">共{{ item.totalNum }}件商品</text>
              <text>实付</text>
              <text class="amount">
                <text class="symbol">¥</text>{{ item.payMoney }}</text
              >
            </view>
            <!-- 订单操作 -->
            <view class="action">
              <view class="button primary">再次购买</view>
              <view class="button">取消订单</view>
            </view>
          </view>
          <!-- 样式美化 -->
          <view class="blank" v-if="orders.items.length === 0">暂无数据~</view>
        </scroll-view>
      </swiper-item>
    </swiper>
  </view>
</template>

<script>
import { OrderStateOptions } from './OrderConstance';
export default {
  data() {
    return {
      // 订单状态描述
      OrderStateOptions,
      // tab栏状态
      orderTabs: [
        {
          orderState: 0,
          title: '全部',
        },
        {
          orderState: 1,
          title: '待付款',
        },
        {
          orderState: 2,
          title: '待发货',
        },
        {
          orderState: 3,
          title: '待收货',
        },
        {
          orderState: 4,
          title: '待评价',
        },
      ],
      // 当前选中的订单状态
      activeIndex: 0,
      // 订单列表
      orderList: new Array(5),
    };
  },
  computed: {
    // 当前显示的订单数据
    currentOrder() {},
  },
};
</script>

<style lang="scss">
page {
  height: 100%;
  overflow: hidden;
}
.viewport {
  height: 100%;
  display: flex;
  flex-direction: column;
  background-color: #fff;
}
.tabs {
  display: flex;
  justify-content: space-around;
  line-height: 60rpx;
  margin: 0 10rpx;
  background-color: #fff;
  box-shadow: 0 4rpx 6rpx rgba(240, 240, 240, 0.6);
  position: relative;
  z-index: 9;
  text {
    flex: 1;
    text-align: center;
    padding: 20rpx;
    font-size: 28rpx;
    color: #262626;
  }
  .cursor {
    position: absolute;
    left: 0;
    bottom: 20rpx;
    display: block;
    content: '';
    width: 20%;
    height: 4rpx;
    padding: 0 50rpx;
    background-clip: content-box;
    background-color: #27ba9b;
    transition: all 0.4s;
  }
}
.orders {
  background-color: #f7f7f8;
  .card {
    min-height: 100rpx;
    padding: 20rpx;
    margin: 20rpx 20rpx 0;
    border-radius: 10rpx;
    background-color: #fff;
    &:last-child {
      padding-bottom: 40rpx;
    }
  }
  .status {
    display: flex;
    align-items: center;
    justify-content: space-between;
    font-size: 28rpx;
    color: #999;
    margin-bottom: 15rpx;
    .date {
      color: #666;
      flex: 1;
    }
    .primary {
      color: #ff9240;
    }
    .icon-delete {
      line-height: 1;
      margin-left: 10rpx;
      padding-left: 10rpx;
      border-left: 1rpx solid #e3e3e3;
    }
  }
  .goods {
    display: flex;
    margin-bottom: 20rpx;
    .cover {
      width: 170rpx;
      height: 170rpx;
      margin-right: 20rpx;
      border-radius: 10rpx;
      overflow: hidden;
      position: relative;
    }
    .quantity {
      position: absolute;
      bottom: 0;
      right: 0;
      line-height: 1;
      padding: 6rpx 4rpx 6rpx 8rpx;
      font-size: 24rpx;
      color: #fff;
      border-radius: 10rpx 0 0 0;
      background-color: rgba(0, 0, 0, 0.6);
    }
    .meta {
      flex: 1;
      display: flex;
      flex-direction: column;
      justify-content: center;
    }
    .name {
      height: 80rpx;
      font-size: 26rpx;
      color: #444;
    }
    .type {
      line-height: 1.8;
      padding: 0 15rpx;
      margin-top: 10rpx;
      font-size: 24rpx;
      align-self: flex-start;
      border-radius: 4rpx;
      color: #888;
      background-color: #f7f7f8;
    }
    .more {
      flex: 1;
      display: flex;
      align-items: center;
      justify-content: center;
      font-size: 22rpx;
      color: #333;
    }
  }
  .payment {
    display: flex;
    justify-content: flex-end;
    align-items: center;
    line-height: 1;
    padding: 20rpx 0;
    text-align: right;
    color: #999;
    font-size: 28rpx;
    border-bottom: 1rpx solid #eee;
    .quantity {
      font-size: 24rpx;
      margin-right: 16rpx;
    }
    .amount {
      color: #444;
      margin-left: 6rpx;
    }
    .symbol {
      font-size: 20rpx;
    }
  }
  .action {
    display: flex;
    flex-direction: row-reverse;
    justify-content: flex-start;
    padding-top: 20rpx;
    .button {
      width: 200rpx;
      height: 60rpx;
      text-align: center;
      line-height: 60rpx;
      margin-left: 20rpx;
      border-radius: 60rpx;
      border: 1rpx solid #ccc;
      font-size: 26rpx;
      color: #444;
    }
    .primary {
      color: #27ba9b;
      border-color: #27ba9b;
    }
  }
  .blank {
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
    font-size: 30rpx;
    margin-top: -40rpx;
    color: #666;
  }
}
</style>

```

### 标题

![image-20221204131039541](assets/image-20221204131039541.png)



### 内容

![image-20221204131128048](assets/image-20221204131128048.png)





### 业务

1. 打开页面 默认加载全部订单信息
2. 当切换tab栏时，自动获取和显示对应的订单信息
3. 执行分页时，判断有没有下一页数据，满足条件才开始执行分页逻辑



参考代码

```js
<script>
import { getMemberOrder } from "@/api/order";
import { OrderConstance } from "./OrderConstance";
export default {
  data() {
    return {
      // 订单列表
      // 全部 待付款  待发货 待收货 待评价
      orderList: new Array(5),
      // tab栏状态
      orderTabs: [
        {
          orderState: 0,
          title: "全部",
        },
        {
          orderState: 1,
          title: "待付款",
        },
        {
          orderState: 2,
          title: "待发货",
        },
        {
          orderState: 3,
          title: "待收货",
        },
        {
          orderState: 4,
          title: "待评价",
        },
      ],
      // 当前选中的订单状态
      activeIndex: 0,
    };
  },
  onLoad() {
    this.getMemberOrder();
  },
  methods: {
    // 加载订单数据  给orderList 第一次 赋值的时候使用
    // 根据当前选中的下标 来切换 要操作的对象
    async getMemberOrder() {
      const data = {
        page: 1,
        pageSize: 10,
        // orderState: OrderState.QuanBu, // 全部
        orderState: this.orderTabs[this.activeIndex].orderState,
      };
      const result = await getMemberOrder(data);
      // 把数据 设置到  orderList 中
      // this.orderList[0] = result.result; // vue对动态添加的数据 没有办法监控到
      this.$set(this.orderList, this.activeIndex, result.result);
    },
    // 轮播组件 切换事件
    swiperChange(event) {
      // 发现当前orderList 对应的下标 的 元素  还没有数据 才发送请求

      // 1 设置 activeIndex 下标
      this.activeIndex = event.detail.current;
      if (!this.orderList[this.activeIndex]) {
        // 没有数据 才发请求
        // 2 发送请求 获取数据
        this.getMemberOrder();
      }
    },
    // 分页的事件
    async onScrolltolower() {
      /*
      1 先判断一下有没有下一页数据
      2 没有 弹出提示
      3 有
        1 页码 + 1
        2 重新发送请求
        3 数据回来， 把新旧数据 合并
       */
      // 获取 当前页码和总页数
      const { page, pages } = this.orderList[this.activeIndex];
      // 判断有没有下一页
      if (page >= pages) {
        return uni.showToast({ title: "没有更多数据", icon: "none" });
      } else {
        // 还有下一页数据
        const data = {
          page: page + 1,
          pageSize: 10,
          orderState: this.orderTabs[this.activeIndex].orderState,
        };
        // 发送请求
        const result = await getMemberOrder(data);
        // 数据回来了 需要做合并！！
        // 对 旧数据page做修改
        this.orderList[this.activeIndex].page = result.result.page;
        // 对 旧数据 items 合并 新旧数组的合并
        this.orderList[this.activeIndex].items = [
          ...this.orderList[this.activeIndex].items,
          ...result.result.items,
        ];
      }
    },
  },
};
</script>
```



