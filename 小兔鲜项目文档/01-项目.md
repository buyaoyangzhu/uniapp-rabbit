# 接口文档

https://www.apifox.cn/apidoc/shared-0e6ee326-d646-41bd-9214-29dbf47648fa/api-43426903

# 项目介绍

## 简介

小兔鲜儿是一个以生鲜类产品为主营业务的电商项目

小兔鲜儿技术栈：uni-app、vue2、vuex、uni-ui

## 功能介绍

小兔鲜儿是一个以生鲜类产品为主营业务的电商项目，由商品模块、

用户模块以及订单模块 3 大核心模块构成，每个模块下又包含了若干

具体的功能业务。

![functions.png](./assets/functions.jpg)

## 技术选型

小兔鲜儿项目是以 uni-app 为技术栈的微信小程序，其中包含了动

画、自定义组件、网络通信、本地存储、微信支付等诸多技术要素

![frames](./assets/frames.jpg)

# 项目准备

## 为 @ 添加代码提示

`jsconfig.json`    `   "@/*": ["src/*"]`

```json
{
  "compilerOptions": {
    "types": ["@dcloudio/types", "miniprogram-api-typings", "mini-types"],
    "baseUrl": ".",
    "paths": {
      "@/*": ["src/*"]
    }
  }
}
```

## vs code 插件准备

### 安装

![image-20221125095726967](./assets/image-20221125095726967.png)



![image-20221125115320539](./assets/image-20221125115320539.png)

# 公共代码

## 引入 vuex

### 引入 vuex

1. 新建文件 `src/store/index.js`

   ```js
   // 页面路径：store/index.js
   import Vue from "vue";
   import Vuex from "vuex";

   Vue.use(Vuex); //vue的插件机制
   //Vuex.Store 构造器选项
   const store = new Vuex.Store({
     state: {},
     getters: {},
     modules: {},
   });
   export default store;
   ```

2. `src/main.js` 中 引入 `store`

   ```js
   import store from "./store";
   
   const app = new Vue({
     store,
     ...App,
   });
   ```

### 引入 vuex 持久化插件

[vuex-uniapp-persistence](https://www.npmjs.com/package/vuex-uniapp-persistence)

1. 下载

   ```
   npm i vuex-uniapp-persistence
   ```

2. 在 store.js 中引入

   ```js
   // 页面路径：store/index.js
   import Vue from "vue";
   import Vuex from "vuex";
   import persistence from "vuex-uniapp-persistence";
   Vue.use(Vuex);
   const store = new Vuex.Store({
     // 引进和开启，注意属性和state同级
     plugins: [persistence()],
     state:{}
   });
   export default store;
   ```

## 封装请求拦截器

1. 新建 `src/utils/http.js`

   ```js
   const baseURL = 'https://pcapi-xiaotuxian-front-devtest.itheima.net';
   const request = {
     invoke(args) {
       uni.showLoading({ title: '加载中' });
       if (!args.url.startsWith('http')) {
         args.url = baseURL + args.url;
       }
       args.header = {
         ...args.header, // 保留原本的 header
         'source-client': 'miniapp', // 添加小程序端调用标识
       };
     },
     complete(res) {
       uni.hideLoading();
     },
   };
   // 对 request 和 uploadFile做了统一的请求前拦截封装
   uni.addInterceptor('request', request);
   uni.addInterceptor('uploadFile', request);
   
   // 对全局请求做了统一的封装，页面要用，就引进 http 直接发送请求即可
   // http({url:'/list'})
   const http = (options) => {
     return new Promise((resolve, reject) => {
       uni.request({
         ...options,
         success(res) {
           if (res.statusCode >= 200 && res.statusCode < 300) {
             resolve(res.data);
           } else {
             reject(res);
           }
         },
         fail(err) {
           reject(err);
         },
       });
     });
   };
   
   export default http;
   
   ```

2. vue组件中使用

   ```vue
   <template>
     <view> </view>
   </template>
   
   <script>
   // 使用 @ 引入组件
   import http from "@/utils/http";
   // console.log(http);
   
   export default {
     async onLoad() {
       const result = await http({ url: "/home/oneStop/mutli" });
       console.log(result);
     },
   };
   </script>
   
   <style></style>
   ```

   ### 封装请求拦截器的意义

   1. 为什么需要自己封装一个http 发送请求的代码

      1. 希望可以封装一个基地址，方便发送请求 
      2. 希望可以实现 发送请求前，自动显示加载中。。。请求结束了，关闭加载中 
      3. 后期 可以在拦截器中  自动携带 登录凭证 token 

   2. 如何封装

      1. 利用 uniapp 提供拦截器来实现封装 

   3. 为什么需要自己基于 uni.request 来封装 一个http 函数 ？

      1. 内部使用 uni.request ，就可以使用上 uniapp的拦截器

      2. 改造一下获取 发送请求后的数据的写法 

           以前：  `const [err,result ]=await uni.request({url:"/banners"})` 

           现在：  `const  result =await http({url:"/banners"})`

## 搭建底部 tabbar

![image-20221124180428135](./assets/image-20221124180428135.png)

```json
  "tabBar": {
    "color": "#333",
    "selectedColor": "#27ba9b",
    "backgroundColor": "#fff",
    "borderStyle": "white",
    "list": [
      {
        "text": "首页",
        "pagePath": "pages/index/index",
        "iconPath": "static/tabs/home_default.png",
        "selectedIconPath": "static/tabs/home_selected.png"
      },
      {
        "text": "分类",
        "pagePath": "pages/category/index",
        "iconPath": "static/tabs/category_default.png",
        "selectedIconPath": "static/tabs/category_selected.png"
      },
      {
        "text": "购物车",
        "pagePath": "pages/cart/index",
        "iconPath": "static/tabs/cart_default.png",
        "selectedIconPath": "static/tabs/cart_selected.png"
      },
      {
        "text": "我的",
        "pagePath": "pages/my/index",
        "iconPath": "static/tabs/user_default.png",
        "selectedIconPath": "static/tabs/user_selected.png"
      }
    ]
  },
```
